package com.example;
import com.sun.org.apache.xpath.internal.operations.Lt;

import javax.imageio.plugins.jpeg.JPEGImageReadParam;
import java.util.*;
import java.util.stream.Collectors;

public class JavaStreamsApplication {

	public static void main(String[] args) {

		List<Person> people = getPeople();

		// Imperative approach ❌
       /*
		List<Person> females = new ArrayList<>();
       for(Person p: people){
       	  if(p.getGender().equals(Gender.FEMALE)){
			  females.add(p);
		  }
	   }
       females.forEach(System.out::println);
      */
		// Declarative approach ✅

		// Filter

		List<Person> females = people.stream()
				.filter(person -> person.getGender().equals(Gender.FEMALE))
				.collect(Collectors.toList());

         //		females.forEach(System.out::println);

		//sort
		List<Person> sorted = people.stream()
				.sorted(Comparator.comparing(Person::getAge).thenComparing(Person::getGender).reversed())
				.collect(Collectors.toList());

		//  sorted.forEach(System.out::println);
       //allMatch
		boolean allMatch = people.stream()
				.allMatch(person -> person.getAge() > 8);

	//	System.out.println(allMatch);
       //anyMatch
        boolean anyMatch = people.stream()
				.anyMatch(person -> person.getAge() > 121);

	//	System.out.println(anyMatch);
       //noneMatch
        boolean noneMatch = people.stream()
				.noneMatch(person -> person.getName().equals("Antonio"));

	//	System.out.println(noneMatch);

		//Max
		people.stream()
				.max(Comparator.comparing(Person::getAge));
			//	.ifPresent(System.out::println);
		// min
		people.stream()
				.min(Comparator.comparing(Person::getAge));
		//.ifPresent(System.out::println);

		//Group

		Map<Gender,List<Person>> groupByGender = people.stream().collect(Collectors.groupingBy(Person::getGender));
        /*
       groupByGender.forEach((gender, people1)->{
		   System.out.println(gender);
		   people1.forEach(System.out::println);
		   System.out.println();
	   });
       */

       Optional<String> oldestFemaleAge = people.stream()
			   .filter(person -> person.getGender().equals(Gender.FEMALE))
			   .max(Comparator.comparing(Person::getAge))
			   .map(Person::getName);

		oldestFemaleAge.ifPresent(System.out::println);
	}

	private static List<Person> getPeople() {
		return Arrays.asList(
				new Person("Antonio", 20, Gender.MALE),
				new Person("Alina Smith", 33, Gender.FEMALE),
				new Person("Helen White", 57, Gender.FEMALE),
				new Person("Alex Boz", 14, Gender.MALE),
				new Person("Jamie Goa", 99, Gender.MALE),
				new Person("Anna Cook", 7, Gender.FEMALE),
				new Person("Zelda Brown", 120, Gender.FEMALE)


		);
	}

}
